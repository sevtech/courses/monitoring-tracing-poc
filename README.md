# Tracing, Logging and Monitoring POC

## Run

At first, you must compile services, gateway and eureka with:

```sh
mvn clean package
```

Create docker network

```sh
docker network create monitoring
```

Up all services:

```sh
docker-compose up --scale orchestrator=2 --build
```

If ELK doesn't work run this:

```sh
sudo sysctl -w vm.max_map_count=262144
```

## Example calls

To generate trances and logs, you can make a call to the service with this commands:

```sh
for ((i=1;i<=1000;i++)); do curl -X GET "http://localhost:8090/customer-orders/101"; done
```

```sh
for ((i=1;i<=1000;i++)); do curl -X GET "http://localhost:8090/customer-orders/102"; done
```

```sh
for ((i=1;i<=1000;i++)); do curl -X GET "http://localhost:8090/customer-orders/103"; done
```

```sh
for ((i=1;i<=1000;i++)); do curl -X GET "http://localhost:8090/customer-orders/104"; done
```

```sh
for ((i=1;i<=1000;i++)); do curl -X GET "http://localhost:8090/customer-orders/105"; done
```

## Services

- API Gateway -> http://localhost:8090
- Eureka -> http://localhost:8761
- Zipkin -> http://localhost:9411
- Kibana -> http://localhost:5601
- Elasticsearch -> http://localhost:9200
- Logstash -> 5044
- Prometheus -> http://localhost:9090
- Grafana -> http://localhost:3000
- Customer Service -> 8082
- Order Service -> 8081
- Orquestator -> 8080

### Grafana

Dashboard JVM: https://grafana.com/grafana/dashboards/4701
Dashboard Zipkin: https://grafana.com/grafana/dashboards/1598

### Elasticsearch

Indices Elasticsearch

```sh
curl http://localhost:9200/_cat/indices
```

## Infraestructure

<a href="https://profile.es/">
    <img src="infra.png" width="600">
</a>

## Future improvements
* Add messaging queue.
* Kubernetes infraestructure.