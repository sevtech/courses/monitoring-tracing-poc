package com.sohan.orchestrator.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrderServiceApplicationTests.class)
public class OrderServiceApplicationTests {

	@Test
	public void contextLoads() {
	}

}
