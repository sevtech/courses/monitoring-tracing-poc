package com.sohan.orchestrator.example.feign.clients;

import com.sohan.order.service.dto.OrderDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "order-service")
public interface OrderClient {

    @GetMapping("/orders/{orderId}")
    OrderDTO findById(@PathVariable("orderId") Integer orderId);

}
