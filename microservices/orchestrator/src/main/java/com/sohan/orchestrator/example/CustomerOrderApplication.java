package com.sohan.orchestrator.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
/**
 * The spring boot application class, that starts the app.
 */
@SpringBootApplication
@EnableFeignClients
public class CustomerOrderApplication {

	/**
	 * Main method that starts the Spring Boot application.
	 *
	 * @param args Arguments passed to the app.
	 */
	public static void main(String[] args) {
		SpringApplication.run(CustomerOrderApplication.class, args);
	}

}
