package com.sohan.orchestrator.example.service.impl;

import com.sohan.orchestrator.example.dto.CustomerDTO;
import com.sohan.orchestrator.example.dto.CustomerOrderDTO;
import com.sohan.orchestrator.example.feign.clients.CustomerClient;
import com.sohan.orchestrator.example.feign.clients.OrderClient;
import com.sohan.orchestrator.example.service.CustomerOrderService;
import com.sohan.order.service.dto.OrderDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service Implementation that fetches / acts on OrderDTO related data.
 *
 * @author Sohan
 */
@Service
public class CustomerOrderServiceImpl implements CustomerOrderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerOrderServiceImpl.class);

    @Autowired
    private CustomerClient customerClient;

    @Autowired
    private OrderClient orderClient;

    /**
     * Gets the Order Details for the given OrderId.
     *
     * @return CustomerOrderDTO
     */
    @Override
    public CustomerOrderDTO getCustomerOrder(Integer orderId) throws Exception {
        CustomerOrderDTO customerOrderDTO = new CustomerOrderDTO();

        LOGGER.info("Fetching Customer and Order details for OrderId: {}", orderId);

        OrderDTO order = getOrder(orderId);

        if (order != null) {
            customerOrderDTO.setOrder(order);
            customerOrderDTO.setCustomer(getCustomer(order.getCustomerId()));
        } else {
            LOGGER.error("No Order found for, OrderId: {}", orderId);
        }

        return customerOrderDTO;
    }

    private OrderDTO getOrder(Integer orderId) {
        return orderClient.findById(orderId);
    }

    private CustomerDTO getCustomer(Integer customerId) {
        return customerClient.findById(customerId);
    }
}
