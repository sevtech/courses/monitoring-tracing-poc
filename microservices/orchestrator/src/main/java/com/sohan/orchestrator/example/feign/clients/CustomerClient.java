package com.sohan.orchestrator.example.feign.clients;

import com.sohan.orchestrator.example.dto.CustomerDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "customer-service")
public interface CustomerClient {

    @GetMapping("/customers/{customerId}")
    CustomerDTO findById(@PathVariable("customerId") Integer customerId);

}
