package com.example.gateway;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class HystrixController {

    @GetMapping("/orchestrator/fallback")
    public Mono<String> orchestrator() {
        return Mono.just("Orchestrator is taking too long to respond or is down. Please try again later");
    }
}
