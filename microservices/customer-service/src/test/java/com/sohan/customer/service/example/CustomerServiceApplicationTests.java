package com.sohan.customer.service.example;

import com.sohan.customer.service.example.CustomerServiceApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CustomerServiceApplication.class)
public class CustomerServiceApplicationTests {

	@Test
	public void contextLoads() {
	}

}
